package de.codementors.assertj;

import org.assertj.core.api.SoftAssertions;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;


public class BasicTests {

    @Test
    void ex1_optionals() {
        Optional<Car> car = CarFactory.createMaybeCar("red");
        //TASK: Check if the car is present
    }

    @Test
    void ex2_optionals() {
        Optional<Car> car = CarFactory.createMaybeCar("green");
        //TASK: Validate, the state of the car battery
    }

    @Test
    void ex3_optionals() {
        Optional<Car> car = CarFactory.createMaybeCar("green");
        Car expectedCar = new Car("green");
        //TASK: Validate, that both cars are equal by same fields (do not change the car implementation!)
    }

    @Test
    void ex4_optionals() {
        List<Car> allCars = new CarPark().getAllCars();
        //TASK: Validate, that all cars except the blue car, have an empty battery. Do not validate, that the blue car has an empty battery :)
    }

    @Test
    void ex5_optionals() {
        List<Car> allCars = new CarPark().getAllCars();
        //TASK: Validate, that all cars except the blue car, have an empty battery, have 4 tires and that their first tire has always a pressure of 0.5
        //Bonus: If the test fails for multiple reasons (e.g. check against 5 tires and pressure 0.6), the test should print the error for ALL attributes of all cars.
    }

    @Test
    void ex6_optionals() {
        Car car = CarFactory.createCar("green");
        //TASK: Validate, that a specific exception with a specific message is thrown by the method readErrorMemory()
    }

    @Test
    void ex7_optionals() {
        Car car = CarFactory.createCar("green");
        //TASK: Validate, that the thrown exception is caused by a ForgotToRefillException
    }

    @Test
    void ex8_string() {
        Car car = CarFactory.createCar("green", "Porsche");
        // TASK: Validate, that the manufacturer name starts with a P.
    }

    @Test
    void ex9_string() {
        Car car = CarFactory.createCar("green", "PoRsChE");
        // TASK: Validate, that the manufacturer is Porsche (ignoring the specific case)
    }

    @Test
    void ex10_lists() {
        List<Car> batteryCars = CarFactory.createBatteryCars();
        //TASK: Validate, that there is a car with a lead battery and exactly two tires
    }

    @Test
    void ex11_lists() {
        List<Person> people = People.people();
        //TASK: Validate, that 'Dr Sheldon Cooper' is part of the people.
        //Hint: There is only one Dr Sheldon Cooper, do not create a second one ;)
    }

    @Test
    void ex12_lists() {
        List<Person> people = People.people();
        //TASK: Validate, that 'Dr Sheldon Cooper' is part of the people.
        Person expectedSheldon = new Person("Sheldon", "Cooper", "Dr");
        //Hint: Now it is time to use his brother ;)
    }

    @Test
    void ex13_lists() {
        Person sheldon = new Person("Sheldon", "Cooper", "Dr");
        //TASK: Make this assertion work

//        PersonAssert.assertThat(sheldon).isSheldon();
    }

    @Test
    void ex15_time() {
        ZonedDateTime seconds = ZonedDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        ZonedDateTime minutes = seconds.truncatedTo(ChronoUnit.MINUTES);
        // TASK: Validate that both are equal based on minute precision
    }

}
