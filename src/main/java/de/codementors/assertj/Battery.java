package de.codementors.assertj;

public class Battery {

    public enum Type{
        LITHIUM, LEAD, NICKEL;
    }

    private Type type;

    public Battery(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
