package de.codementors.assertj;

public class LowOilException extends RuntimeException{
    public LowOilException(String message, ForgotToRefillOilException cause) {
        super(message, cause);
    }
}
