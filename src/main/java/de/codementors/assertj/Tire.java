package de.codementors.assertj;

public class Tire {
    private final int number;
    private double pressure = 1;

    public Tire(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public Tire withPressure(double pressure) {
        this.pressure = pressure;
        return this;
    }

    public double getPressure() {
        return pressure;
    }
}
