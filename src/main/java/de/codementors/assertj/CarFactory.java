package de.codementors.assertj;

import java.util.List;
import java.util.Optional;

public class CarFactory {
    public static Optional<Car> createMaybeCar(String color) {
        return Optional.of(new Car(color));
    }

    public static Car createCar(String color) {
        return new Car(color);
    }

    public static Car createCar(String color, String manufacturer) {
        return new Car(color, manufacturer);
    }

    public static List<Car> createBatteryCars() {
        return List.of(new Car("red").withBattery(Battery.Type.LEAD).withTires(List.of(new Tire(3),new Tire(5))),
                new Car("black").withBattery(Battery.Type.LITHIUM),
                new Car("white").withBattery(Battery.Type.NICKEL));
    }
}
