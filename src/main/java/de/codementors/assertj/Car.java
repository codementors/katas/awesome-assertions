package de.codementors.assertj;

import java.util.List;
import java.util.Optional;

public class Car {
    private final String color;

    private String manufacturer;
    private Battery battery;

    private List<Tire> tires = List.of(new Tire(1).withPressure(0.5), new Tire(2), new Tire(3), new Tire(4));

    public Car(String color) {
        this.color = color;
    }
    public Car(String color, String manufacturer) {
        this.color = color;
        this.manufacturer = manufacturer;
    }

    public String getColor() {
        return color;
    }

    public Optional<Battery> getBattery() {
        return Optional.ofNullable(battery);
    }

    public Car withBattery(Battery.Type type) {
        battery = new Battery(type);
        return this;
    }

    public List<Tire> getTires() {
        return tires;
    }

    public Car withTires(List<Tire> tires){
        this.tires = tires;
        return this;
    }

    public void readErrorMemory() {
        throw new LowOilException("Oil level critical", new ForgotToRefillOilException());
    }

    public String getManufacturer() {
        return manufacturer;
    }


}
