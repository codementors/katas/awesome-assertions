package de.codementors.assertj;

import java.util.List;

public class People {
    public static List<Person> people(){
        return List.of(new Person("Bob", "Ross", null), new Person("Sheldon", "Cooper", "Dr"), new Person("Charles", "Xavier", "Prof"));
    }
}
