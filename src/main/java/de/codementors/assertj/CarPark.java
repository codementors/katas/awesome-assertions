package de.codementors.assertj;

import java.util.List;

public class CarPark {

    List<Car> getAllCars(){
        return List.of(new Car("green"), new Car("red"), new Car("blue").withBattery(Battery.Type.NICKEL));
    }
}
